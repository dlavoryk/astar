#include <iostream>
#include <vector>
#include <list>
#include <set>
#include <stack>
#include <windows.h>

#include "GlobalDefines.h"


#define Red  RGB (255,0,0)
#define Lime RGB (206,255,0)
#define Blue RGB (0,0,255)
#define Green RGB(0, 255, 0)
#define White RGB(255, 255, 255)
#define Yellow RGB(255, 255, 0)
#define Oragne RGB(255, 127, 0)
#define Pink RGB(255, 192, 203)
#define Black RGB(0, 0, 0)

static HWND    hConWnd;
int     BCX_Line(HWND, int, int, int, int, int = 0, HDC = 0);
int     BCX_Circle(HWND, int, int, int, int = 0, int = 0, HDC = 0);
HWND    GetConsoleWndHandle(void);


auto & grid = Map;

// A structure to hold the neccesary parameters 
struct cell
{
	// Row and Column index of its parent 
	// Note that 0 <= i <= ROW-1 & 0 <= j <= COL-1 
	int parent_i, parent_j;
	// f = g + h 
	double f, g, h;
};

// A Utility Function to check whether given cell (row, col) 
// is a valid cell or not. 
bool isValid(int row, int col)
{
	// Returns true if row number and column number 
	// is in range 
	return (row >= 0) && (row < ROW) &&
		(col >= 0) && (col < COL);
}

// A Utility Function to check whether the given cell is 
// blocked or not 
bool isUnBlocked(const int grid[][COL], int row, int col)
{
	// Returns true if the cell is not blocked else false 
	return grid[row][col] == 1;
}

// A Utility Function to check whether destination cell has 
// been reached or not 
bool isDestination(int row, int col, const Pair & dest)
{
	return row == dest.first && col == dest.second;
}

// A Utility Function to calculate the 'h' heuristics. 
double calculateHValue(int row, int col, const Pair & dest)
{
	// Return using the distance formula 
//	return D * ((double)sqrt((row - dest.first) * (row - dest.first) + (col - dest.second) * (col - dest.second)));
//	return 0;
	return D * (abs(row - dest.first) + abs(col - dest.second));
//	return 0;

}

// TODO: Add body
void tracePath(cell cellDetails[ROW][COL], Pair dest)
{
	printf("\nThe Path is ");
	int row = dest.first;
	int col = dest.second;

	int Arr[ROW][COL];
	memset(Arr, 0, sizeof(Arr));


	stack<Pair> Path;
	while (!(cellDetails[row][col].parent_i == row
		&& cellDetails[row][col].parent_j == col))
	{
		Path.push(make_pair(row, col));
		const int temp_row = cellDetails[row][col].parent_i;
		const int temp_col = cellDetails[row][col].parent_j;
		row = temp_row;
		col = temp_col;
	}
	Path.push(make_pair(row, col));
	int ii = 0;
	while (!Path.empty())
	{
		pair<int, int> p = Path.top();
		Path.pop();
		Arr[p.first][p.second] = ++ii;
		printf("-> (%d,%d) ", p.first, p.second);
	}


	cout << endl;
	for (auto& el : Arr)
	{
		for (int j = 0; j < COL; ++j)
		{
			cout << el[j] << " ";
			if (el[j] < 10)
			{
				cout << " ";
			}
		}
		cout << endl;
	}
}

template<typename T>
T sqr(T x)
{
	return x * x;
}

double Distance(double x1, double y1, double x2, double y2)
{
	return (abs(x1 - x2) + abs(y1 - y2));
//	return sqrt(sqr(x1 - x2) + sqr(y1 - y2));
}



void Draw(int CurI, int CurJ)
{
	decltype(White) Colors[] =
	{
		Black, White, Blue, Green, Yellow, Pink, Red
	};

	BCX_Circle(hConWnd, START_X + CurJ * STEP_X, START_Y + CurI * STEP_Y, RADIUS, Colors[MapCount[CurI][CurJ]], true);

	Sleep(SLEEP_TIME);
}




void AStarSearch(const int Map[ROW][COL], const Pair & Source, const Pair & Destination)
{
	if (isDestination(Source.first, Source.second, Destination))
	{
		cout << "Already In destination\n";
		return;
	}
	if (!isValid(Source.first, Source.second))
	{
		cout << "Source is invalid\n";
		return;
	}
	if (!isValid(Destination.first, Destination.second))
	{
		cout << "Source is invalid\n";
		return;
	}

	bool closedList[ROW][COL];
	memset(closedList, false, sizeof(closedList));
	cell cellDetails[ROW][COL];

	int i, j;

	for (i = 0; i < ROW; i++)
	{
		for (j = 0; j < COL; j++)
		{
			cellDetails[i][j].f = MAX_DIST;
			cellDetails[i][j].g = MAX_DIST;
			cellDetails[i][j].h = MAX_DIST;
			cellDetails[i][j].parent_i = -1;
			cellDetails[i][j].parent_j = -1;
		}
	}
	i = Source.first, j = Source.second;
	cellDetails[i][j].f = 0.0;
	cellDetails[i][j].g = 0.0;
	cellDetails[i][j].h = 0.0;
	cellDetails[i][j].parent_i = i;
	cellDetails[i][j].parent_j = j;

	set<pPair> openList;
	hConWnd = GetConsoleWndHandle();

	// Put the starting cell on the open list and set its 
	// 'f' as 0 
	openList.insert(make_pair(0.0, make_pair(i, j)));

	// We set this boolean value as false as initially 
	// the destination is not reached. 
	bool foundDest = false;

	while (!openList.empty())
	{
		pPair p = *openList.begin();

		// Remove this vertex from the open list 
		openList.erase(openList.begin());
		i = p.second.first;
		j = p.second.second;
		closedList[i][j] = true;
		/*
		Generating all the 8 successor of this cell

		N.W   N   N.E
		\   |   /
		\  |  /
		W----Cell----E
		/ | \
		/   |  \
		S.W    S   S.E

		Cell-->Popped Cell (i, j)
		N -->  North       (i-1, j)
		S -->  South       (i+1, j)
		E -->  East        (i, j+1)
		W -->  West           (i, j-1)
		N.E--> North-East  (i-1, j+1)
		N.W--> North-West  (i-1, j-1)
		S.E--> South-East  (i+1, j+1)
		S.W--> South-West  (i+1, j-1)*/

		// To store the 'g', 'h' and 'f' of the 8 successors 
	//	double gNew, hNew, fNew;

		for (auto & Offset : Offsets)
		{
			const int CurI = i + Offset.first, CurJ = j + Offset.second;
			// there is no point to chech invalid Node
			if (isValid(CurI, CurJ))
			{
				if (isDestination(CurI, CurJ, Destination))
				{
					cellDetails[CurI][CurJ].parent_i = i;
					cellDetails[CurI][CurJ].parent_j = j;
					cout << "\nPath was found\n";
					MapCount[CurI][CurJ]++;
					tracePath(cellDetails, Destination);
					return;
				}
				if (!closedList[CurI][CurJ] && isUnBlocked(Map, CurI, CurJ))
				{////////////////////////////////////// We could Add more logic in this function
					const double gNew = cellDetails[i][j].g + Distance(i, j, CurI, CurJ);
					const double hNew = calculateHValue(CurI, CurJ, Destination);
					const double fNew = gNew + hNew;
					// If it isn�t on the open list, add it to 
					// the open list. Make the current square 
					// the parent of this square. Record the 
					// f, g, and h costs of the square cell 
					//                OR 
					// If it is on the open list already, check 
					// to see if this path to that square is better, 
					// using 'f' cost as the measure. 
					MapCount[CurI][CurJ]++;

					Draw(CurI, CurJ);


					if (abs(cellDetails[CurI][CurJ].f - MAX_DIST) < 0.05 || cellDetails[CurI][CurJ].f > fNew)
					{
						openList.insert(make_pair(fNew, make_pair(CurI, CurJ)));
						cellDetails[CurI][CurJ].f = fNew;
						cellDetails[CurI][CurJ].g = gNew;
						cellDetails[CurI][CurJ].h = hNew;
						cellDetails[CurI][CurJ].parent_i = i;
						cellDetails[CurI][CurJ].parent_j = j;
					}
				}
			}
		}
	}
	if (!foundDest)
	{
		printf("Failed to find the Destination Cell\n");
	}


}



int main()
{
	getchar();  // wait
	MapCount[Start.first][Start.second]++;

	for (auto & Row : Map)
	{
		for (auto & El : Row)
		{
			std::cout << (El == 0 ? "*" : " ");
		}
		std::cout << endl;
	}
	
	AStarSearch(Map, Start, Destination);
	hConWnd = GetConsoleWndHandle();
	if (hConWnd)
	{
		//BCX_Circle(hConWnd, 3, 5, 5, Blue, true);
		//BCX_Circle(hConWnd, 3, 5, 5, Blue, true);
		//BCX_Circle(hConWnd, 13, 15, 5, Blue, true);
		//BCX_Circle(hConWnd, 5, 185, 5, Blue, true);
		//BCX_Circle(hConWnd, 5, 169, 5, Blue, true);
		//BCX_Circle(hConWnd, 5, 153, 5, Blue, true);
		//BCX_Circle(hConWnd, 5, 137, 5, Blue, true);
		//BCX_Circle(hConWnd, 5, 57, 5, Blue, true);
		//BCX_Circle(hConWnd, 28, 57, 5, Blue, true);

		//for (int i = 0; i < ROW; ++i)
		//{
		//	for (int j = 0; j < COL; ++j)
		//	{

		//		BCX_Circle(hConWnd, START_X + j * STEP_X, START_Y + i * STEP_Y, RADIUS, RGB(MapCount[i][j] * 50, MapCount[i][j] * 0, MapCount[i][j] * 0), true);// RGB(MapCount[i][j] * 100, 0, 0), true);

		//	}
		//}


	}

	for (int i = 0; i < ROW; ++i)
	{
		for (int j = 0; j < COL; ++j)
		{
	//		std::cout << MapCount[i][j] << " ";
		}
	//	std::cout << std::endl;
	}
	getchar();  // wait
}




int BCX_Line(HWND Wnd, int x1, int y1, int x2, int y2, int Pen, HDC DrawHDC)
{
	int a, b = 0;
	HPEN hOPen;
	// penstyle, width, color
	HPEN hNPen = CreatePen(PS_SOLID, 2, Pen);
	if (!DrawHDC) DrawHDC = GetDC(Wnd), b = 1;
	hOPen = (HPEN)SelectObject(DrawHDC, hNPen);
	// starting point of line
	MoveToEx(DrawHDC, x1, y1, NULL);
	// ending point of line
	a = LineTo(DrawHDC, x2, y2);
	DeleteObject(SelectObject(DrawHDC, hOPen));
	if (b) ReleaseDC(Wnd, DrawHDC);
	return a;
}
// converts circle(centerX,centerY,radius,pen) to WinApi function
// ellipse inside box with upper left and lower right coordinates
int BCX_Circle(HWND Wnd, int X, int Y, int R, int Pen, int Fill, HDC DrawHDC)
{
	int a, b = 0;
	if (!DrawHDC) DrawHDC = GetDC(Wnd), b = 1;
	// penstyle, width, color
	HPEN   hNPen = CreatePen(PS_SOLID, 2, Pen);
	HPEN   hOPen = (HPEN)SelectObject(DrawHDC, hNPen);
	HBRUSH hOldBrush;
	HBRUSH hNewBrush;
	// if true will fill circle with pencolor
	if (Fill)
	{
		hNewBrush = CreateSolidBrush(Pen);
		hOldBrush = (HBRUSH)SelectObject(DrawHDC, hNewBrush);
	}
	else
	{
		hNewBrush = (HBRUSH)GetStockObject(NULL_BRUSH);
		hOldBrush = (HBRUSH)SelectObject(DrawHDC, hNewBrush);
	}
	a = Ellipse(DrawHDC, X - R, Y + R, X + R, Y - R);
	DeleteObject(SelectObject(DrawHDC, hOPen));
	DeleteObject(SelectObject(DrawHDC, hOldBrush));
	if (b) ReleaseDC(Wnd, DrawHDC);
	return a;
}
// the hoop ...
HWND GetConsoleWndHandle(void)
{
	HWND hConWnd;
	_OSVERSIONINFOEXW os;
	wchar_t szTempTitle[64], szClassName[128], szOriginalTitle[1024];
	os.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
	VerifyVersionInfo(&os, NULL, NULL);
	// may not work on WIN9x
	if (os.dwPlatformId == VER_PLATFORM_WIN32s) return 0;
	GetConsoleTitle(szOriginalTitle, sizeof(szOriginalTitle));
	wprintf(szTempTitle, "%u - %u", GetTickCount(), GetCurrentProcessId());
	SetConsoleTitle(szTempTitle);
	Sleep(40);
	// handle for NT
	hConWnd = FindWindow(NULL, szTempTitle);
	SetConsoleTitle(szOriginalTitle);
	// may not work on WIN9x
	if (os.dwPlatformId == VER_PLATFORM_WIN32_WINDOWS)
	{
		hConWnd = GetWindow(hConWnd, GW_CHILD);
		if (hConWnd == NULL)  return 0;
		GetClassName(hConWnd, szClassName, sizeof(szClassName));
		while (wcscmp(szClassName, L"ttyGrab") != 0)
		{
			hConWnd = GetNextWindow(hConWnd, GW_HWNDNEXT);
			if (hConWnd == NULL)  return 0;
			GetClassName(hConWnd, szClassName, sizeof(szClassName));
		}
	}
	return hConWnd;
}